# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_inliststring, d_string, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, imageOutput
# #}
# add your imports before the next gnomon tag

from copy import deepcopy

import numpy as np

from timagetk import SpatialImage, MultiChannelImage

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten

@algorithmPlugin(version="0.1.0", coreversion="1.0.0", name="Image channel operation")
@imageInput("img", data_plugin="gnomonImageDataMultiChannelImage")
@imageOutput("out_img", data_plugin="gnomonImageDataMultiChannelImage")
class imageChannelOperation(gnomon.core.gnomonAbstractFormAlgorithm):
    """Create a new channel as a basic combination of two existing ones

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['channel_a'] = d_inliststring('Channel A', "", [""], "Channel to use as the first term of the operation")
        self._parameters['factor_a'] = d_real('Factor A', 1., 0, 10, 2, "Multiplicative factor to apply to the first term")
        self._parameters['channel_b'] = d_inliststring('Channel B', "", [""], "Channel to use as the second term of the operation")
        self._parameters['factor_b'] = d_real('Factor B', 1., 0, 10, 2, "Multiplicative factor to apply to the second term")
        self._parameters['operation'] = d_inliststring("Operation", "difference", ["sum", "difference", "product", "ratio"], "Type of operation to apply")
        self._parameters['result_channel'] = d_string("Result Channel", "", "Channel name to assign to the result image (may replace an existing one)")

        self.img = {}
        self.out_img = {}

    def run(self):
        self.out_img = {}
        for time in self.img.keys():
            img = self.img[time]
            # #}
            # implement the run method

            img_a = img.get_channel(self['channel_a'])
            img_b = img.get_channel(self['channel_b'])
            operation_symbols = {"sum": "+", "difference": "-", "product": "*", "ratio": "/"}
            operation_functions = {
                "sum": np.sum,
                "difference": np.diff,
                "product": np.prod,
                "ratio": lambda a, axis : np.exp(np.diff(np.log(a), axis=axis))
            }
            array_a = self['factor_a'] * img_a.get_array().astype(float)
            array_b = self['factor_b'] * img_b.get_array().astype(float)
            result = operation_functions[self['operation']]([array_b, array_a], axis=0)[0]

            result = np.minimum(result, np.iinfo(img_a.dtype).max)
            result = np.maximum(result, np.iinfo(img_a.dtype).min)
            result = result.astype(img_a.dtype)

            operation_result_channel = f"{self['channel_a']}{operation_symbols[self['operation']]}{self['channel_b']}"
            result_name = self['result_channel'] if self['result_channel'] != "" else operation_result_channel

            img_dict = {c: deepcopy(img.get_channel(c)) for c in img.channel_names}
            result_img = SpatialImage(result, voxelsize=img.voxelsize)
            img_dict.update({result_name: result_img})

            self.out_img[time] = MultiChannelImage(img_dict)

    def refreshParameters(self):
        if len(self.img) > 0:
            img = list(self.img.values())[0]
            for i, parameter_name in enumerate(['channel_a', 'channel_b']):
                channel_name = self[parameter_name]
                self._parameters[parameter_name].setValues(img.channel_names)
                if channel_name in img.channel_names:
                    self._parameters[parameter_name].setValue(channel_name)
                else:
                    self._parameters[parameter_name].setValue(img.channel_names[np.minimum(i, len(img.channel_names))])

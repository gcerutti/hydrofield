import logging
from copy import deepcopy

import numpy as np
import pandas as pd

from dtkcore import d_real, d_bool, d_inliststring, d_inliststringlist

import gnomon.core
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import pointCloudInput, imageInput, pointCloudOutput, dataFrameOutput

from maps_2d.signal_map import SignalMap
from maps_2d.epidermal_maps import compute_local_2d_signal, nuclei_density_function
from maps_2d.signal_map_analysis import signal_map_regions

from sam_atlas.centering import optimize_vertical_axis

@algorithmPlugin(version="0.4.0", coreversion="1.0.0", name="Property-based SAM Centering")
@pointCloudInput("df", data_plugin="gnomonPointCloudDataPandas")
@pointCloudOutput('out_df', data_plugin="gnomonPointCloudDataPandas")
@dataFrameOutput('data_df', data_plugin="gnomonDataFrameDataPandas")
class propertySamCentering(gnomon.core.gnomonAbstractFormAlgorithm):
    """Compute the centered positions of SAM cells using a point cloud property

    """

    def __init__(self):
        super().__init__()

        self.df = {}
        self.out_df = {}
        self.data_df = {}

        self._parameters = {}
        self._parameters['property'] = d_inliststring("Property", "", [""], "Point property to use to center the SAM")
        self._parameters['threshold'] = d_real("Threshold", 1, 0, 100, 2, "Threshold used to determine the central area")
        self._parameters['alignment_times'] = d_inliststringlist("Centering Times", [], [], "Which time points to use to compute the centering transformation")
        self._parameters['microscope_orientation'] = d_inliststring("Microscope Orientation", "upright", ["upright", "inverted"], "The orientation of the microscope used to acquire images")

        self.connectParameter("property")

    def refreshParameters(self):
        if len(self.df)>0:
            sorted_times = list(np.sort([str(t) for t in self.df.keys()]))
            print(sorted_times)

            if len(sorted_times) == 1:
                if 'alignment_times' in self._parameters.keys():
                    del self._parameters['alignment_times']
            else:
                if 'alignment_times' not in self._parameters.keys():
                    self._parameters['alignment_times'] = d_inliststringlist("Centering Times", [], [], "Which time points to use to compute the centering transformation")
                    times = sorted_times
                else:
                    times = self['alignment_times'] if len(self['alignment_times'])>0 else sorted_times
                self._parameters['alignment_times'].setValues(sorted_times)
                self._parameters['alignment_times'].setValue(times)

            df = list(self.df.values())[0]
            property_name = self['property']
            prop = list(df.columns)
            prop =[p for p in prop if not np.array(df[p]).dtype == np.dtype('O')]
            prop = [p for p in prop if np.array(df[p]).ndim == 1]
            self._parameters['property'].setValues(prop)
            self._parameters['property'].setValue(property_name if property_name in prop else prop[0])

            self._update_threshold()

    def onParameterChanged(self, parameter_name):
        if parameter_name == 'property':
            self._update_threshold()

    def _update_threshold(self):
        if len(self.df)>0:
            df = list(self.df.values())[0]
            point_properties = df[self['property']].values
    
            min_p = float(np.around(np.nanmin(point_properties), decimals=3))
            max_p = float(np.around(np.nanmax(point_properties), decimals=3))
    
            self._parameters['threshold'].setMin(min_p)
            self._parameters['threshold'].setMax(max_p)
            self._parameters['threshold'].setValue((min_p+max_p)/2)

    def run(self):
        self.data_df = {}

        self.out_df = {}

        if 'alignment_times' in self._parameters.keys():
            alignment_times = [float(t) for t in self['alignment_times']]
        else:
            alignment_times = [float(t) for t in self.df]

        registration_data = pd.concat([self.df[t].query("layer==1") for t in alignment_times])

        r_max=120
        microscope_orientation = -1 if (self['microscope_orientation'] == 'inverted') else 1

        signal_map = SignalMap(
            registration_data, position_name='registered',
            origin=(r_max, r_max), extent=r_max, resolution=2, polar=False,
            radius=5., density_k=0.25
        )

        meristem_regions = signal_map_regions(signal_map, self['property'], threshold=self['threshold'])
        meristem_region = meristem_regions.iloc[np.argmax(meristem_regions['area'])]
        meristem_center = meristem_region[['center_x', 'center_y']].values

        X = registration_data['registered_x'].values
        Y = registration_data['registered_y'].values
        Z = registration_data['registered_z'].values

        center_altitude = compute_local_2d_signal(np.transpose([X, Y]), meristem_center, Z)[0]
        centered_positions = np.transpose([X, Y, Z]) - np.array(list(meristem_center) + [center_altitude])

        rotated_positions, rotation_matrix = optimize_vertical_axis(centered_positions, r_max=r_max)

        for time, df in self.df.items():
            out_df = deepcopy(df)

            centered_positions = out_df[[f'registered_{dim}' for dim in 'xyz']] - np.array(list(meristem_center) + [center_altitude])
            rotated_positions = np.einsum('...ij,...j->...i', rotation_matrix, centered_positions)

            out_df['centered_x'] = rotated_positions[:, 0]
            out_df['centered_y'] = rotated_positions[:, 1]
            out_df['centered_z'] = microscope_orientation*rotated_positions[:, 2]

            out_df['radial_distance'] = np.linalg.norm([rotated_positions[:, 0], rotated_positions[:, 1]], axis=0)

            self.out_df[time] = out_df
            self.data_df[time] = deepcopy(out_df)

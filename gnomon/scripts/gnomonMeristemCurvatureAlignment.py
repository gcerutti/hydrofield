import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView


def initialize_plugins():
    load_plugin_group('cellImageFilter')
    load_plugin_group('cellImageQuantification')
    load_plugin_group('cellImageReader')
    load_plugin_group('cellImageVtkVisualization')
    load_plugin_group('dataFrameWriter')
    load_plugin_group('imageReader')
    load_plugin_group('imageFilter')
    load_plugin_group('imageVtkVisualization')
    load_plugin_group('meshReader')
    load_plugin_group('meshFilter')
    load_plugin_group('meshVtkVisualization')
    load_plugin_group('pointCloudFromImage')
    load_plugin_group('pointCloudQuantification')
    load_plugin_group('pointCloudReader')
    load_plugin_group('pointCloudVtkVisualization')
    load_plugin_group('pointCloudWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default="/projects/SamTransport")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    parser.add_argument('-t', '--times', default=[0], nargs='+', help='List of individual time points to process', type=int)
    parser.add_argument('-r', '--results-directory', help='Relative path to directory storing the results [default : results/]', default=None)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : microscopy/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)

    args = parser.parse_args()

    initialize_plugins()

    project_path = args.project_path
    filename = args.filename
    file_times = args.times
    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "microscopy/"
    microscopy_dirname = f"{project_path}/{microscopy_directory}"
    channel_names = args.channel_names # ["MCTP2", "PI"]

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    # paths = [f"{microscopy_dirname}/{filename}-T{str(t).zfill(2)}.czi" for t in file_times]
    paths = [f"{microscopy_dirname}/{filename}-t{str(t).zfill(1)}.czi" for t in file_times]
    paths = [f for f in paths if os.path.exists(f)]
    if len(paths) == 0:
        paths = [f"{microscopy_dirname}/{filename}.czi" for t in file_times[:1]]
        paths = [f for f in paths if os.path.exists(f)]
        file_times = [0]
    path = "".join([f",{p}" for p in paths])[1:]
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["Ch1", "ChS1"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()

    channel_colormaps = {'TagBFP': '0RGB_blue', 'PI': 'gray', 'CLV3': '0CMY_magenta', 'DIIV': '0CMY_yellow'}
    reporter_names = [c for c in channel_names if c not in ['TagBFP', 'PI', 'DIIV', 'CLV3']]
    channel_colormaps.update({c: '0CMY_cyan' for c in reporter_names})

    if not os.path.exists(f"{output_dirname}/{filename}/image"):
        os.makedirs(f"{output_dirname}/{filename}/image")
    view = gnomonStandaloneVtkView()
    for channel in channel_names:
        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", channel)
        visu.setParameter("colormap", channel_colormaps[channel])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.2)
        visu.setParameter("contrast", 0.5)
        visu.update()
        visu.setVisible(True)

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}-T{str(t).zfill(2)}_image.png")

    assert os.path.exists(f"{output_dirname}/{filename}/segmentation")
    paths = [f"{output_dirname}/{filename}/segmentation/{filename}-T{str(t).zfill(2)}_PI_seg.inr.gz" for t in file_times]
    path = "".join([f",{p}" for p in paths])[1:]
    reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
    reader.setPath(path)
    reader.run()

    view = gnomonStandaloneVtkView()

    visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
    visu.setView(view)
    visu.setCellImage(reader.cellImage())
    visu.refreshParameters()
    visu.update()

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}-T{str(t).zfill(2)}_segmentation.png")

    if not os.path.exists(f"{output_dirname}/{filename}/surface_mesh"):
        meshing = gnomon.core.meshFromImage_pluginFactory().create("segmentationSurfaceMesh")
        meshing.setCellImage(reader.cellImage())
        meshing.refreshParameters()
        meshing.setParameter("orientation", "down")
        meshing.setParameter("padding", True)
        meshing.setParameter("resampling_voxelsize", 0.66)
        meshing.run()

        curvature = gnomon.core.meshFilter_pluginFactory().create("triangleMeshCurvature")
        curvature.setInput(meshing.output())
        curvature.refreshParameters()
        curvature.run()

        os.makedirs(f"{output_dirname}/{filename}/surface_topomesh")
        writer = gnomon.core.meshWriter_pluginFactory().create("gnomonMeshWriterPropertyTopomesh")
        mesh = curvature.output()
        for i_t, (t, (g_t, m)) in enumerate(zip(file_times, mesh.items())):
            writer.setMesh({t: m})
            writer.setPath(f"{output_dirname}/{filename}/surface_topomesh/{filename}-T{str(t).zfill(2)}_surface_topomesh.ply")
            writer.run()

    paths = [f"{output_dirname}/{filename}/surface_mesh/{filename}-T{str(t).zfill(2)}_surface_topomesh.ply" for t in file_times]
    path = "".join([f",{p}" for p in paths])[1:]
    mesh_reader = gnomon.core.meshReader_pluginFactory().create("gnomonMeshReaderPropertyTopomesh")
    mesh_reader.setPath(path)
    mesh_reader.run()

    meristem = gnomon.core.meshFilter_pluginFactory().create("meshMeristemDelineation")
    meristem.setInput(mesh_reader.mesh())
    meristem.refreshParameters()
    meristem.run()

    segmentation = gnomon.core.meshFilter_pluginFactory().create("vertexPropertyWatershedSegmentation")
    segmentation.setInput(meristem.output())
    segmentation.refreshParameters()
    segmentation.setParameter("property", "principal_curvature_min")
    segmentation.setParameter("region_type", "maxima")
    segmentation.setParameter("gaussian_sigma", 5.)
    segmentation.setParameter("merge_regions", True)
    segmentation.setParameter("merge_threshold", 0.015)
    segmentation.setParameter("label_property", "segmentation")
    segmentation.run()

    colormaps = {'mean_curvature': 'curvature', 'meristem': 'temperature', 'segmentation': 'glasbey'}
    value_ranges = {'mean_curvature': (-0.05, 0.05), 'meristem': (0, 1), 'segmentation': (0, 255)}
    for property_name in ['mean_curvature', 'meristem', 'segmentation']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.meshVtkVisualization_pluginFactory().create("meshVisualizationPyvista")
        visu.setView(view)
        visu.setMesh(segmentation.output())
        visu.refreshParameters()
        visu.setParameter("tube_edges", False)
        visu.setParameter("position", "barycenter")
        visu.setParameter("sphere_vertices", False)
        visu.setParameter("scalars", property_name)
        visu.setParameter("value_range", array_real_2(value_ranges[property_name]))
        visu.setParameter("colormap",colormaps[property_name])
        visu.update()

        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.update()
        visu.setVisible(False)

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view.updateBounds()
            view.setCameraXY(True, True)
            view.saveScreenshot(f"{output_dirname}/{filename}/surface_mesh/{filename}-T{str(t).zfill(2)}_{property_name}_surface_mesh.png")

    cell_projection = gnomon.core.meshFilter_pluginFactory().create("surfaceMeshCellProjection")
    cell_projection.setCellImage(reader.cellImage())
    cell_projection.setInput(segmentation.output())
    cell_projection.refreshParameters()
    cell_projection.setParameter("sampling_depth", 4.)
    cell_projection.setParameter("method", "first")
    cell_projection.setParameter("remove_isolated_labels", False)
    cell_projection.run()

    view = gnomonStandaloneVtkView()

    visu = gnomon.visualization.meshVtkVisualization_pluginFactory().create("meshVisualizationPyvista")
    visu.setView(view)
    visu.setMesh(cell_projection.output())
    visu.refreshParameters()
    visu.setParameter("tube_edges", False)
    visu.setParameter("position", "barycenter")
    visu.setParameter("sphere_vertices", False)
    visu.setParameter("scalars", 'display_label')
    visu.setParameter("value_range", array_real_2((0, 255)))
    visu.setParameter("colormap", "glasbey")
    visu.update()

    visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
    visu.setView(view)
    visu.setImage(rename.output())
    visu.refreshParameters()
    visu.update()
    visu.setVisible(False)

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/surface_mesh/{filename}-T{str(t).zfill(2)}_surface_mesh_labels.png")

    layer = gnomon.core.cellImageQuantification_pluginFactory().create("surfaceMeshCellLayer")
    layer.setCellImage(reader.cellImage())
    layer.setMesh(cell_projection.output())
    layer.refreshParameters()
    layer.run()

    colormaps.update({'layer': 'glasbey'})
    value_ranges.update({'layer': (0, 255)})
    for property_name in ['layer']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
        visu.setView(view)
        visu.setCellImage(layer.outputCellImage())
        visu.refreshParameters()
        visu.setParameter("property_name", property_name)
        visu.setParameter("colormap", colormaps[property_name])
        visu.setParameter("value_range", array_real_2(value_ranges[property_name]))
        visu.update()

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view.updateBounds()
            view.setCameraXY(True, True)
            view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}-T{str(t).zfill(2)}_segmentation_{property_name}.png")

    mesh_property = gnomon.core.cellImageQuantification_pluginFactory().create("surfaceMeshCellProperty")
    mesh_property.setCellImage(layer.outputCellImage())
    mesh_property.setMesh(cell_projection.output())
    mesh_property.refreshParameters()
    mesh_property.setParameter("attribute_names", ['meristem', 'segmentation', 'normal', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max'])
    mesh_property.run()

    closing = gnomon.core.cellImageFilter_pluginFactory().create("cellBinaryPropertyOperation")
    closing.setInput(mesh_property.outputCellImage())
    closing.refreshParameters()
    closing.setParameter("property_name", 'meristem')
    closing.setParameter("method", 'closing')
    closing.setParameter("layer", '1')
    closing.setParameter("iterations", 3)
    closing.run()

    majority = gnomon.core.cellImageFilter_pluginFactory().create("cellPropertyMajorityFilter")
    majority.setInput(closing.output())
    majority.refreshParameters()
    majority.setParameter("property_name", 'segmentation')
    majority.setParameter("neighborhood", 1)
    majority.setParameter("layer", '1')
    majority.setParameter("iterations", 3)
    majority.run()

    if not os.path.exists(f"{output_dirname}/{filename}/cell_data"):
        os.makedirs(f"{output_dirname}/{filename}/cell_data")
    for property_name in ['mean_curvature', 'meristem', 'segmentation']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
        visu.setView(view)
        visu.setCellImage(majority.output())
        visu.refreshParameters()
        visu.setParameter("property_name", property_name)
        visu.setParameter("colormap", colormaps[property_name])
        visu.setParameter("value_range", array_real_2(value_ranges[property_name]))
        visu.setParameter("filter_name", 'layer')
        visu.setParameter("filter_range", array_real_2([0.5, 1.5]))
        visu.update()

        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.update()
        visu.setVisible(False)

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view.updateBounds()
            view.setCameraXY(True, True)
            view.saveScreenshot(f"{output_dirname}/{filename}/cell_data/{filename}-T{str(t).zfill(2)}_L1_cell_{property_name}.png")

    cell_center = gnomon.core.pointCloudFromImage_pluginFactory().create("cellCentersTimagetk")
    cell_center.setCellImage(majority.output())
    cell_center.refreshParameters()
    cell_center.setParameter('cell_properties', ['meristem', 'segmentation', 'layer', 'normal', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max'])
    cell_center.run()

    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = cell_center.output()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/cell_data/{filename}-T{str(t).zfill(2)}_cell_data.csv")
        writer.run()

    registration = gnomon.core.pointCloudQuantification_pluginFactory().create("nucleiRigidRegistration")
    registration.setPointCloud(cell_center.output())
    registration.setImage(rename.output())
    registration.refreshParameters()
    registration.setParameter("registration_channel", "PI")
    registration.run()

    if not os.path.exists(f"{output_dirname}/{filename}/registered_data"):
        os.makedirs(f"{output_dirname}/{filename}/registered_data")
    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = registration.outputPointCloud()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/registered_data/{filename}-T{str(t).zfill(2)}_registered_cell_data.csv")
        writer.run()

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "propertySamCentering"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    centering = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    centering.setInputPointCloud(registration.outputPointCloud())
    centering.refreshParameters()
    centering.setParameter("property", "meristem")
    centering.setParameter("threshold", 0.5)
    centering.setParameter("microscope_orientation", "inverted")
    centering.run()

    alignment = gnomon.core.pointCloudQuantification_pluginFactory().create('curvatureSamAlignment')
    alignment.setPointCloud(centering.outputPointCloud())
    alignment.refreshParameters()
    alignment.setParameter("pz_radius", 60.)
    alignment.run()

    if not os.path.exists(f"{output_dirname}/{filename}/aligned_data"):
        os.makedirs(f"{output_dirname}/{filename}/aligned_data")
    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = alignment.outputPointCloud()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/aligned_data/{filename}-T{str(t).zfill(2)}_aligned_nuclei_data.csv")
        writer.run()

    for column in ['mean_curvature']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.pointCloudVtkVisualization_pluginFactory().create("pointCloudVtkVisualization")
        visu.setView(view)
        visu.setPointCloud(alignment.outputPointCloud())
        visu.refreshParameters()
        visu.setParameter("scale_factor", 3.)
        visu.setParameter("position", "aligned")
        visu.setParameter("property", column)
        visu.setParameter("value_range", array_real_2(value_ranges[column]))
        visu.setParameter("colormap", colormaps[column])
        visu.update()

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view.updateBounds()
            view.setAxesVisible(True)
            view.setCameraXY(False, False)
            view.saveScreenshot(f"{output_dirname}/{filename}/aligned_data/{filename}-T{str(t).zfill(2)}_aligned_{column}_nuclei.png")

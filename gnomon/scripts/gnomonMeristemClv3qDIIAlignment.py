import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView

def initialize_plugins():
    load_plugin_group('imageReader')
    load_plugin_group('imageFilter')
    load_plugin_group('pointCloudQuantification')
    load_plugin_group('pointCloudReader')
    load_plugin_group('pointCloudVtkVisualization')
    load_plugin_group('pointCloudWriter')
    load_plugin_group('dataFrameWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default="/projects/SamTransport")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    parser.add_argument('-t', '--times', default=[0], nargs='+', help='List of individual time points to process', type=int)
    parser.add_argument('-r', '--results-directory', help='Relative path to directory storing the results [default : results/]', default=None)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : microscopy/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)
    parser.add_argument('-o', '--sam-orientation', help='Clockwise or counterclockwise orientation of the SAM', default="clockwise", choices=["clockwise", "counterclockwise"])

    args = parser.parse_args()
    initialize_plugins()

    project_path = args.project_path
    filename = args.filename
    file_times = args.times
    sam_orientation = args.sam_orientation # "clockwise"
    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "microscopy/"
    microscopy_dirname = f"{project_path}/{microscopy_directory}"
    channel_names = args.channel_names

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    paths = [f"{microscopy_dirname}/{filename}-T{str(t).zfill(2)}.tif" for t in file_times]
    path = "".join([f",{p}" for p in paths])[1:]
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["Ch1", "ChS1"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()

    point_reader = gnomon.core.pointCloudReader_pluginFactory().create("gnomonPointCloudReaderDataFrame")
    paths = [
        f"{output_dirname}/{filename}/nuclei_data/{filename}-T{str(t).zfill(2)}_nuclei_data.csv"
        for t in file_times
    ]
    path = "".join([f",{p}" for p in paths])[1:]
    point_reader.setPath(path)
    point_reader.run()

    registration = gnomon.core.pointCloudQuantification_pluginFactory().create("nucleiRigidRegistration")
    registration.setPointCloud(point_reader.pointCloud())
    registration.setImage(rename.output())
    registration.refreshParameters()
    registration.setParameter("registration_channel", "PI")
    registration.run()

    alignment = gnomon.core.pointCloudQuantification_pluginFactory().create("clv3qDIISamAlignment")
    alignment.setPointCloud(registration.outputPointCloud())
    alignment.refreshParameters()
    alignment.setParameter("sam_orientation", sam_orientation)
    alignment.setParameter("microscope_orientation", 'inverted')
    alignment.setParameter("alignment_times", [str(i_t) for i_t, t in enumerate(file_times) if t<=12])
    alignment.run()

    if not os.path.exists(f"{output_dirname}/{filename}/aligned_data"):
        os.makedirs(f"{output_dirname}/{filename}/aligned_data")
    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = alignment.outputPointCloud()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/aligned_data/{filename}-T{str(t).zfill(2)}_aligned_nuclei_data.csv")
        writer.run()

    colormaps = {'qDII': '1Flashy_green', 'DR5': '1Flashy_turquoise', 'CLV3': '1Flashy_purple', 'mean_curvature': 'temperature'}
    value_ranges = {'qDII': (0, 1), 'DR5': (0, 25000), 'CLV3': (0, 25000), 'mean_curvature': (-0.05, 0.05)}

    for column in ['qDII', 'DR5', 'CLV3', 'mean_curvature']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.pointCloudVtkVisualization_pluginFactory().create("pointCloudVtkVisualization")
        visu.setView(view)
        visu.setPointCloud(alignment.outputPointCloud())
        visu.refreshParameters()
        visu.setParameter("scale_factor", 3.)
        visu.setParameter("position", "aligned")
        visu.setParameter("property", column)
        visu.setParameter("value_range", array_real_2(value_ranges[column]))
        visu.setParameter("colormap", colormaps[column])
        visu.update()

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view.updateBounds()
            view.setAxesVisible(True)
            view.setCameraXY(True, True)
            view.saveScreenshot(f"{output_dirname}/{filename}/aligned_data/{filename}-T{str(t).zfill(2)}_aligned_{column}_nuclei.png")

    primordia_detection = gnomon.core.pointCloudQuantification_pluginFactory().create("spiralPhyllotaxisPrimordiaDetection")
    primordia_detection.setPointCloud(alignment.outputPointCloud())
    primordia_detection.refreshParameters()
    primordia_detection.run()

    writer = gnomon.core.pointCloudWriter_pluginFactory().create("pointCloudWriterDataFrame")
    point_cloud = primordia_detection.outputPointCloud()
    for i_t, (t, (g_t, p_c)) in enumerate(zip(file_times, point_cloud.items())):
        writer.setPointCloud({t: p_c})
        writer.setPath(f"{output_dirname}/{filename}/aligned_data/{filename}-T{str(t).zfill(2)}_aligned_primordia_data.csv")
        writer.run()

import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView


def initialize_plugins():
    load_plugin_group('cellImageFromImage')
    load_plugin_group('cellImageQuantification')
    load_plugin_group('cellImageVtkVisualization')
    load_plugin_group('cellImageWriter')
    load_plugin_group('imageFilter')
    load_plugin_group('imageReader')
    load_plugin_group('imageVtkVisualization')
    load_plugin_group('meshFromImage')
    load_plugin_group('meshFilter')
    load_plugin_group('meshVtkVisualization')
    load_plugin_group('meshWriter')
    load_plugin_group('dataFrameWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default="/projects/SamTransport")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    parser.add_argument('-t', '--times', default=[0], nargs='+', help='List of individual time points to process', type=int)
    parser.add_argument('-r', '--results-directory', help='Relative path to directory storing the results [default : results/]', default=None)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : microscopy/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)
    args = parser.parse_args()

    initialize_plugins()

    project_path = args.project_path
    filename = args.filename
    file_times = args.times
    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "microscopy/"
    microscopy_dirname = f"{project_path}/{microscopy_directory}"
    channel_names = args.channel_names # ["MCTP2", "PI"]

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    paths = [f"{microscopy_dirname}/{filename}-T{str(t).zfill(2)}.tif" for t in file_times]
    path = "".join([f",{p}" for p in paths])[1:]
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["Ch1", "ChS1"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()

    if not os.path.exists(f"{output_dirname}/{filename}/image"):
        os.makedirs(f"{output_dirname}/{filename}/image")
    channel_colormaps = {'TagBFP': '0RGB_blue', 'PI': 'gray', 'CLV3': '0CMY_magenta', 'DIIV': '0CMY_yellow'}
    channel_colormaps.update({c: '0CMY_cyan' for c in channel_names if c not in channel_colormaps})

    view = gnomonStandaloneVtkView()
    for channel in channel_names:
        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", channel)
        visu.setParameter("colormap", channel_colormaps[channel])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.2)
        visu.setParameter("contrast", 0.5)
        visu.update()
        visu.setVisible(True)

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}-T{str(t).zfill(2)}_image.png")

        view.setCameraXZ(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}-T{str(t).zfill(2)}_image_XZ.png")

        view.setCameraYZ(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}-T{str(t).zfill(2)}_image_YZ.png")

    segmentation = gnomon.core.cellImageFromImage_pluginFactory().create("autoSeededWatershedSegmentation")
    segmentation.setInput(rename.output())
    segmentation.refreshParameters()
    segmentation.setParameter("membrane_channel", "PI")
    segmentation.setParameter("gaussian_sigma", 0.33)
    segmentation.setParameter("seg_gaussian_sigma", 0.33)
    segmentation.setParameter("h_min", 2)
    segmentation.setParameter("max_volume", 0)
    segmentation.setParameter("padding", True)
    segmentation.setParameter("orientation", "down")
    segmentation.run()

    volume = gnomon.core.cellImageQuantification_pluginFactory().create("cellFeaturesTissueImage")
    volume.setCellImage(segmentation.output())
    volume.refreshParameters()
    volume.run()

    if not os.path.exists(f"{output_dirname}/{filename}/segmentation"):
        os.makedirs(f"{output_dirname}/{filename}/segmentation")
    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    cell_image = volume.outputCellImage()
    for i_t, (t, (g_t, c_i)) in enumerate(zip(file_times, cell_image.items())):
        writer.setCellImage({t: c_i})
        writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}-T{str(t).zfill(2)}_PI_seg.inr.gz")
        writer.run()

    view = gnomonStandaloneVtkView()

    visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
    visu.setView(view)
    visu.setCellImage(volume.outputCellImage())
    visu.refreshParameters()
    visu.update()

    for i_t, t in enumerate(file_times):
        view.setCurrentTime(i_t)
        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}-T{str(t).zfill(2)}_segmentation.png")

    meshing = gnomon.core.meshFromImage_pluginFactory().create("segmentationSurfaceMesh")
    meshing.setCellImage(volume.outputCellImage())
    meshing.refreshParameters()
    meshing.setParameter("orientation", "down")
    meshing.setParameter("padding", True)
    meshing.setParameter("resampling_voxelsize", 0.66)
    meshing.run()

    layer = gnomon.core.cellImageQuantification_pluginFactory().create("surfaceMeshCellLayer")
    layer.setCellImage(volume.outputCellImage())
    layer.setMesh(meshing.output())
    layer.refreshParameters()
    layer.run()

    curvature = gnomon.core.meshFilter_pluginFactory().create("triangleMeshCurvature")
    curvature.setInput(meshing.output())
    curvature.refreshParameters()
    curvature.run()

    if not os.path.exists(f"{output_dirname}/{filename}/surface_topomesh"):
        os.makedirs(f"{output_dirname}/{filename}/surface_topomesh")
    writer = gnomon.core.meshWriter_pluginFactory().create("gnomonMeshWriterPropertyTopomesh")
    mesh = curvature.output()
    for i_t, (t, (g_t, m)) in enumerate(zip(file_times, mesh.items())):
        writer.setMesh({t: m})
        writer.setPath(f"{output_dirname}/{filename}/surface_topomesh/{filename}-T{str(t).zfill(2)}_surface_topomesh.ply")
        writer.run()

    colormaps = {'mean_curvature': 'temperature'}
    value_ranges = {'mean_curvature': (-0.05, 0.05)}
    for property_name in ['mean_curvature']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.meshVtkVisualization_pluginFactory().create("meshVisualizationTopomesh")
        visu.setView(view)
        visu.setMesh(curvature.output())
        visu.refreshParameters()
        visu.setParameter("tube_edges", True)
        visu.setParameter("edge_color", "lightgrey")
        visu.setParameter("attribute", property_name)
        visu.setParameter("value_range", array_real_2(value_ranges[property_name]))
        visu.setParameter("colormap", colormaps[property_name])
        visu.update()

        visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
        visu.setView(view)
        visu.setCellImage(volume.outputCellImage())
        visu.refreshParameters()
        visu.setParameter("opacity", 0.)
        visu.update()

        for i_t, t in enumerate(file_times):
            view.setCurrentTime(i_t)
            view.updateBounds()
            view.setCameraXY(True, True)
            view.saveScreenshot(f"{output_dirname}/{filename}/surface_topomesh/{filename}-T{str(t).zfill(2)}_surface_mesh_{property_name}.png")

    mesh_property = gnomon.core.cellImageQuantification_pluginFactory().create("surfaceMeshCellProperty")
    mesh_property.setCellImage(layer.outputCellImage())
    mesh_property.setMesh(curvature.output())
    mesh_property.refreshParameters()
    mesh_property.setParameter("attribute_names", ['normal', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max'])
    mesh_property.run()

    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    cell_image = mesh_property.outputCellImage()
    for i_t, (t, (g_t, c_i)) in enumerate(zip(file_times, cell_image.items())):
        writer.setCellImage({t: c_i})
        writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}-T{str(t).zfill(2)}_PI_seg.inr.gz")
        writer.run()

    writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
    data_frame = mesh_property.outputDataFrame()
    for i_t, (t, (g_t, d_f)) in enumerate(zip(file_times, data_frame.items())):
        writer.setDataFrame({t: d_f})
        writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}-T{str(t).zfill(2)}_cell_data.csv")
        writer.run()

import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView


def initialize_plugins():
    load_plugin_group('cellImageFromImage')
    load_plugin_group('cellImageQuantification')
    load_plugin_group('cellImageVtkVisualization')
    load_plugin_group('cellImageWriter')
    load_plugin_group('imageFilter')
    load_plugin_group('imageReader')
    load_plugin_group('imageVtkVisualization')
    load_plugin_group('imageWriter')
    load_plugin_group('meshFromImage')
    load_plugin_group('meshFilter')
    load_plugin_group('meshVtkVisualization')
    load_plugin_group('meshWriter')
    load_plugin_group('dataFrameWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default="/projects/SamTransport")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    # parser.add_argument('-t', '--times', default=[0], nargs='+', help='List of individual time points to process', type=int)
    parser.add_argument('-r', '--results-directory', help='Relative path to directory storing the results [default : results/]', default=None)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : microscopy/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)
    args = parser.parse_args()

    initialize_plugins()

    project_path = args.project_path
    filename = args.filename
    # file_times = args.times
    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "microscopy/"
    microscopy_dirname = f"{project_path}/{microscopy_directory}"
    channel_names = args.channel_names # ["V-N7", "PI", 'TQ-SV40"]

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    path = f"{microscopy_dirname}/{filename}.czi"
    # path = f"{microscopy_dirname}/{filename}_CROP.tif"
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["Ch1-T1", "Ch2-T1", "ChS1-T2"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()


    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "imageChannelOperation"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    channel_operation = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    channel_operation.setInputImage(rename.output())
    channel_operation.refreshParameters()
    channel_operation.setParameter("channel_a", "PI")
    channel_operation.setParameter("factor_a", 1.)
    channel_operation.setParameter("channel_b", "V-N7")
    channel_operation.setParameter("factor_b", 0.5)
    channel_operation.setParameter("operation", "difference")
    channel_operation.setParameter("result_channel", "PI-Clean")
    channel_operation.run()
    channel_names += ["PI-Clean"]

    if not os.path.exists(f"{output_dirname}/{filename}/image"):
        os.makedirs(f"{output_dirname}/{filename}/image")

    channel_colormaps = {'PI': 'gray', 'PI-Clean': 'gray', 'TQ-SV40': '0CMY_cyan', 'V-N7': '0CMY_yellow'}
    for channel in channel_names:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(channel_operation.outputImage())
        visu.refreshParameters()
        visu.setParameter("channel", channel)
        visu.setParameter("colormap", channel_colormaps[channel])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 1)
        visu.setParameter("brightness", 1)
        visu.setParameter("contrast", 1)
        visu.update()
        visu.setVisible(True)

        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}_{channel}_image.png")

    # image_writer = gnomon.core.imageWriter_pluginFactory().create("gnomonImageWriter")
    # image_writer.setImage(channel_operation.outputImage())
    # image_writer.setPath(f"{output_dirname}/{filename}/image/{filename}_clean_PI.tif")
    # image_writer.run()

    histogram_equalization = gnomon.core.imageFilter_pluginFactory().create("equalizeAdaptHist")
    histogram_equalization.setInput(channel_operation.outputImage())
    histogram_equalization.refreshParameters()
    histogram_equalization.setParameter("channels", ['PI-Clean'])
    histogram_equalization.setParameter("kernel_size", 0.2)
    histogram_equalization.setParameter("clip_limit", 0.01)
    histogram_equalization.run()

    segmentation = gnomon.core.cellImageFromImage_pluginFactory().create("autoSeededWatershedSegmentation")
    segmentation.setInput(histogram_equalization.output())
    segmentation.refreshParameters()
    segmentation.setParameter("membrane_channel", "PI-Clean")
    # segmentation.setParameter("gaussian_sigma", 0.5)
    segmentation.setParameter("gaussian_sigma", 0.75)
    # segmentation.setParameter("seg_gaussian_sigma", 0.33)
    segmentation.setParameter("seg_gaussian_sigma", 0.5)
    # segmentation.setParameter("h_min", 2)
    # segmentation.setParameter("h_min", 300)
    segmentation.setParameter("h_min", 500)
    # segmentation.setParameter("h_min", 1000)
    segmentation.setParameter("max_volume", 0)
    segmentation.setParameter("padding", True)
    segmentation.setParameter("orientation", "down")
    segmentation.run()

    if not os.path.exists(f"{output_dirname}/{filename}/segmentation"):
        os.makedirs(f"{output_dirname}/{filename}/segmentation")
    view = gnomonStandaloneVtkView()

    visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
    visu.setView(view)
    visu.setCellImage(segmentation.output())
    visu.refreshParameters()
    visu.update()

    view.updateBounds()
    view.setCameraXY(True, True)
    view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmentation.png")

    visu.refreshParameters()
    visu.setParameter("y_range", array_real_2([sum(visu['y_range'])/2, visu['y_range'][1]]))
    visu.update()
    view.setCameraXZ(True, True)
    view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmentation_XZ.png")

    visu.refreshParameters()
    visu.setParameter("x_range", array_real_2([sum(visu['x_range'])/2, visu['x_range'][1]]))
    visu.update()
    view.setCameraYZ(True, True)
    view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmentation_YZ.png")

    volume = gnomon.core.cellImageQuantification_pluginFactory().create("cellFeaturesTissueImage")
    volume.setCellImage(segmentation.output())
    volume.refreshParameters()
    volume.run()

    meshing = gnomon.core.meshFromImage_pluginFactory().create("segmentationSurfaceMesh")
    meshing.setCellImage(volume.outputCellImage())
    meshing.refreshParameters()
    meshing.setParameter("orientation", "down")
    meshing.setParameter("padding", True)
    meshing.setParameter("resampling_voxelsize", 0.66)
    meshing.run()

    layer = gnomon.core.cellImageQuantification_pluginFactory().create("surfaceMeshCellLayer")
    layer.setCellImage(volume.outputCellImage())
    layer.setMesh(meshing.output())
    layer.refreshParameters()
    layer.run()

    property_colormaps = {'layer': 'jet'}
    property_ranges = {'layer': (0, 4)}
    for property_name in ['layer']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
        visu.setView(view)
        visu.setCellImage(layer.outputCellImage())
        visu.refreshParameters()
        visu.setParameter('property_name', property_name)
        visu.update()
        visu.setParameter('colormap', property_colormaps[property_name])
        visu.setParameter('value_range', array_real_2(property_ranges[property_name]))
        visu.update()

        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_cell_{property_name}.png")

        visu.refreshParameters()
        visu.setParameter("y_range", array_real_2([sum(visu['y_range'])/2, visu['y_range'][1]]))
        visu.setParameter('property_name', property_name)
        visu.update()
        visu.setParameter('colormap', property_colormaps[property_name])
        visu.setParameter('value_range', array_real_2(property_ranges[property_name]))
        visu.update()
        view.setCameraXZ(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_cell_{property_name}_XZ.png")

        visu.refreshParameters()
        visu.setParameter("x_range", array_real_2([sum(visu['x_range'])/2, visu['x_range'][1]]))
        visu.setParameter('property_name', property_name)
        visu.update()
        visu.setParameter('colormap', property_colormaps[property_name])
        visu.setParameter('value_range', array_real_2(property_ranges[property_name]))
        visu.update()
        view.setCameraYZ(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_cell_{property_name}_YZ.png")

    curvature = gnomon.core.meshFilter_pluginFactory().create("triangleMeshCurvature")
    curvature.setInput(meshing.output())
    curvature.refreshParameters()
    curvature.run()

    if not os.path.exists(f"{output_dirname}/{filename}/surface_mesh"):
        os.makedirs(f"{output_dirname}/{filename}/surface_mesh")
    mesh_writer = gnomon.core.meshWriter_pluginFactory().create("gnomonMeshWriterPropertyTopomesh")
    mesh_writer.setMesh(curvature.output())
    mesh_writer.setPath(f"{output_dirname}/{filename}/surface_mesh/{filename}_surface_topomesh.ply")
    mesh_writer.run()

    property_colormaps.update({'mean_curvature': 'temperature'})
    property_ranges.update({'mean_curvature': (-0.05, 0.05)})
    for property_name in ['mean_curvature']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.meshVtkVisualization_pluginFactory().create("meshVisualizationTopomesh")
        visu.setView(view)
        visu.setMesh(curvature.output())
        visu.refreshParameters()
        visu.setParameter("tube_edges", True)
        visu.setParameter("edge_color", "lightgrey")
        visu.setParameter("attribute", property_name)
        visu.setParameter("value_range", array_real_2(property_ranges[property_name]))
        visu.setParameter("colormap", property_colormaps[property_name])
        visu.update()

        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/surface_mesh/{filename}_surface_mesh_{property_name}.png")

    mesh_property = gnomon.core.cellImageQuantification_pluginFactory().create("surfaceMeshCellProperty")
    mesh_property.setCellImage(layer.outputCellImage())
    mesh_property.setMesh(curvature.output())
    mesh_property.refreshParameters()
    mesh_property.setParameter("attribute_names", ['normal', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max'])
    mesh_property.run()

    cell_signal = gnomon.core.cellImageQuantification_pluginFactory().create("signalQuantificationImageSignal")
    cell_signal.setCellImage(mesh_property.outputCellImage())
    cell_signal.setImage(rename.output())
    cell_signal.refreshParameters()
    cell_signal.setParameter("channels", ['TQ-SV40', 'V-N7'])
    cell_signal.setParameter("erosion_radius", 0)
    cell_signal.run()

    property_colormaps.update({'TQ-SV40': '0CMY_cyan', 'V-N7': '0CMY_yellow'})
    property_ranges.update({'TQ-SV40': (0, 40000), 'V-N7': (0, 40000), })
    for property_name in ['TQ-SV40', 'V-N7', 'mean_curvature']:
        view = gnomonStandaloneVtkView()

        visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
        visu.setView(view)
        visu.setCellImage(cell_signal.outputCellImage())
        visu.refreshParameters()
        visu.setParameter('property_name', property_name)
        visu.update()
        visu.setParameter('colormap', property_colormaps[property_name])
        visu.setParameter('value_range', array_real_2(property_ranges[property_name]))
        visu.update()

        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_cell_{property_name}.png")

    cellimage_writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    cellimage_writer.setCellImage(cell_signal.outputCellImage())
    cellimage_writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}_PI_seg.inr.gz")
    cellimage_writer.run()

    dataframe_writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
    dataframe_writer.setDataFrame(cell_signal.outputDataFrame())
    dataframe_writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}_cell_data.csv")
    dataframe_writer.run()
